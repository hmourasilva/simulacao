# SIMULADOR DE PARCELAS SELIC
> Simulador de compra com taxa de juros selic.

Serviço REST que simula compra de produto aplicando compra parcelada com taxa de juros SELIC


## Exemplo de uso

Baixe o projeto e execute os passos abaixo:

* 1
    * Execute o comando maven no diretorio raiz do projeto

```sh
mvn clean instal
```

* 2
    * execute o comando abaixo no diretorio target do projeto'

```sh
java -jar simulacao-0.0.1.jar
```

* 3
    * o serviço REST será exposto na porta 8080

```sh
http://localhost:8080/swagger-ui.html
```

## Detalhes

Para executar o serviço parcelas basta efetuar requisição POST informando os dados de compra para o endpoint abaixo
```sh
http://localhost:8080/v1/api/pagamento/parcelas
```

Abaixo está um exemplo de request e response do serviço


```sh
{
	"produto": {
		"codigo": 123,
		"nome": "Nome do Produto",
		"valor": 1000.00
	},
	"condicaoPagamento": {
		"valorEntrada": 200.00,
		"qtdeParcelas": 8
	}
}
```

```sh
[
    {
        "numeroParcela": 1,
        "valor": 100.00,
        "valorAcrescido": 0,
        "taxaJurosAoMes": 0
    },
    {
        "numeroParcela": 2,
        "valor": 100.00,
        "valorAcrescido": 0,
        "taxaJurosAoMes": 0
    },
    {
        "numeroParcela": 3,
        "valor": 100.00,
        "valorAcrescido": 0,
        "taxaJurosAoMes": 0
    },
    {
        "numeroParcela": 4,
        "valor": 100.00,
        "valorAcrescido": 0,
        "taxaJurosAoMes": 0
    },
    {
        "numeroParcela": 5,
        "valor": 100.00,
        "valorAcrescido": 0,
        "taxaJurosAoMes": 0
    },
    {
        "numeroParcela": 6,
        "valor": 100.00,
        "valorAcrescido": 0,
        "taxaJurosAoMes": 0
    },
    {
        "numeroParcela": 7,
        "valor": 100.00,
        "valorAcrescido": 111.00,
        "taxaJurosAoMes": 0.11
    },
    {
        "numeroParcela": 8,
        "valor": 100.00,
        "valorAcrescido": 111.00,
        "taxaJurosAoMes": 0.11
    }
]
}
```



## TAXA SELIC

Este serviço utiliza e consulta a base de taxa selic informada pelo BCB (Taxa de juros - Selic acumulada no mês) 

```sh
https://dadosabertos.bcb.gov.br/dataset/4390-taxa-de-juros---selic-acumulada-no-mes
```

package br.com.hmoura.simulacao;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.hmoura.simulacao.compra.model.Parcela;
import br.com.hmoura.simulacao.compra.service.ParcelasServico;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SimulacaoApplicationTests {

	@Autowired
	private ParcelasServico parcelaServico;

	@Test
	public void contextLoads() {
	}

	@Test
	public void calculaValorParcelaTest() {
		BigDecimal resultadoEsperado = new BigDecimal(100);
		log.info("resultadoEsperado: " + resultadoEsperado);

		BigDecimal valorParaParcelar = new BigDecimal(800);
		log.info("valorParaParcelar: " + valorParaParcelar);

		Integer qtdeParcelas = 8;
		log.info("qtdeParcelas: " + qtdeParcelas);

		BigDecimal resultado = this.parcelaServico.calculaValorParcela(valorParaParcelar, qtdeParcelas);
		log.info("resultado: " + resultado);

		assertThat(resultado).isEqualTo(resultadoEsperado);
	}

	@Test
	public void calculaParcelasTest() {
		BigDecimal valorParcela = new BigDecimal(150);
		log.info("valorParcela: " + valorParcela);

		List<Parcela> parcelas = this.parcelaServico.calculaParcelas(10, valorParcela);
		log.info("parcelas: " + parcelas.size());
		
		assertThat(parcelas.size()).isEqualTo(10);
		assertThat(parcelas.get(0).getValor()).isEqualTo(valorParcela);
	}
}

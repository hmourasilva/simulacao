package br.com.hmoura.simulacao.compra.selic.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Selic {

	public Selic() {
		super();
	}
	
	public Selic(String data, String valor) {
		super();
		this.data = data;
		this.valor = valor;
	}

	private String data;
	private String valor;
	
	public BigDecimal getValorDecimal() {
		if(!valor.isEmpty()) {
			return new BigDecimal(this.valor);
		}		
		return new BigDecimal(0);
	}
	
	public BigDecimal getFatorMultiplicacao() {
		return new BigDecimal(1).add(this.getValorDecimal());
	}
}

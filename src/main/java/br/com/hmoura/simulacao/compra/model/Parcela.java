package br.com.hmoura.simulacao.compra.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Parcela {

	public Parcela() {
		super();
	}

	public Parcela(Integer numeroParcela, BigDecimal valor, BigDecimal valorAcrescido, BigDecimal taxaJurosAoMes) {
		super();
		this.numeroParcela = numeroParcela;
		this.valor = valor;
		this.valorAcrescido = valorAcrescido;
		this.taxaJurosAoMes = taxaJurosAoMes;
	}

	private Integer numeroParcela;
	private BigDecimal valor;
	private BigDecimal valorAcrescido;
	private BigDecimal taxaJurosAoMes;

	public Integer getNumeroParcela() {
		if (numeroParcela == null) {
			numeroParcela = 0;
		}
		return numeroParcela;
	}

	public BigDecimal getValor() {
		if (valor == null) {
			valor = new BigDecimal(0);
		}
		return valor;
	}

	public BigDecimal getValorAcrescido() {
		if (valorAcrescido == null) {
			valorAcrescido = new BigDecimal(0);
		}
		return valorAcrescido;
	}

	public BigDecimal getTaxaJurosAoMes() {
		if (taxaJurosAoMes == null) {
			taxaJurosAoMes = new BigDecimal(0);
		}
		return taxaJurosAoMes;
	}
}

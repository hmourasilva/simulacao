package br.com.hmoura.simulacao.compra.selic.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hmoura.simulacao.compra.selic.model.Selic;

@Service
public class SelicServico {

	@Autowired
	private SelicCliente cliente;
	
	public Selic obtemSelicAtual() {
		List<Selic> listaResultados = cliente.buscaValoreAtual();
		if(listaResultados != null && listaResultados.size() > 0) {
			return listaResultados.get(0);
		}		
		return new Selic();
	}
	
	public BigDecimal obterValorAtual() {
		Selic selic = this.obtemSelicAtual();
		return selic.getValorDecimal();
	}
}

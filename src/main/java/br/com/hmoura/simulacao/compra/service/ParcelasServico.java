package br.com.hmoura.simulacao.compra.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hmoura.simulacao.compra.model.Compra;
import br.com.hmoura.simulacao.compra.model.Parcela;
import br.com.hmoura.simulacao.compra.selic.model.Selic;
import br.com.hmoura.simulacao.compra.selic.service.SelicServico;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ParcelasServico {

	@Autowired
	private SelicServico selicServico;

	public static int INICIO_CALCULO_JUROS_PADRAO = 6;

	/**
	 * @param compra
	 * @return
	 */
	public List<Parcela> calulaParcelas(Compra compra) {

		Selic selic = this.selicServico.obtemSelicAtual();
		log.info("VALOR SELIC: " + selic.getValorDecimal() + " DATA SELIC: " + selic.getData());

		Integer qtdParcelas = compra.getCondicaoPagamento().getQtdeParcelas();
		log.info("qtdParcelas: " + qtdParcelas);

		BigDecimal valorParcela = this.calculaValorParcela(compra);
		log.info("valorParcela: " + valorParcela);

		List<Parcela> parcelas = this.calculaParcelas(qtdParcelas, valorParcela);// monta lista de parcelas
		this.calculaJuros(parcelas, selic, INICIO_CALCULO_JUROS_PADRAO);// calcula o juros para as parcelas maiores que
																		// o inicio de calculo

		return parcelas;
	}

	private BigDecimal calculaValorParcela(Compra compra) {
		BigDecimal valorParcela = this.calculaValorParcela(compra.getValorParaParcelar(),
				compra.getCondicaoPagamento().getQtdeParcelas());
		return valorParcela;
	}

	public BigDecimal calculaValorParcela(BigDecimal valorParaParcelar, Integer qtdParcelas) {
		BigDecimal valorParcela = new BigDecimal(0);
		if (valorParaParcelar != null && qtdParcelas != null) {
			valorParcela = valorParaParcelar.divide(new BigDecimal(qtdParcelas));
		}
		return valorParcela;
	}

	/**
	 * @param qtdParcelas
	 * @param valorParcela
	 * @return
	 */
	public List<Parcela> calculaParcelas(Integer qtdParcelas, BigDecimal valorParcela) {
		List<Parcela> parcelas = new ArrayList<>();
		for (int i = 1; i <= qtdParcelas; i++) {
			Parcela parcela = new Parcela();
			parcela.setNumeroParcela(i);
			parcela.setValor(valorParcela);
			parcelas.add(parcela);
		}
		return parcelas;
	}

	/**
	 * @param parcelas
	 * @param selic
	 * @param inicioCalculoJuros
	 * @return
	 */
	public List<Parcela> calculaJuros(List<Parcela> parcelas, Selic selic, int inicioCalculoJuros) {

		if (parcelas != null && parcelas.size() > inicioCalculoJuros) {
			for (int i = inicioCalculoJuros; i < parcelas.size(); i++) {

				Parcela parcela = parcelas.get(i);
				parcela.setTaxaJurosAoMes(selic.getValorDecimal());
				parcela.setValorAcrescido(parcela.getValor().multiply(selic.getFatorMultiplicacao()).setScale(2));
			}
		}
		return parcelas;
	}
}

package br.com.hmoura.simulacao.compra.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Produto {

	public Produto() {
		super();
	}
	
	public Produto(Long codigo, String nome, BigDecimal valor) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.valor = valor;
	}
	
	private Long codigo;
	private String nome;
	private BigDecimal valor;
	
	public BigDecimal getValor() {
		if(valor == null) {
			valor = new BigDecimal(0);
		}
		return valor;
	}
	
	
}

package br.com.hmoura.simulacao.compra.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CondicaoPagamento {

	public CondicaoPagamento() {
		super();
	}
	
	public CondicaoPagamento(BigDecimal valorEntrada, Integer qtdeParcelas) {
		super();
		this.valorEntrada = valorEntrada;
		this.qtdeParcelas = qtdeParcelas;
	}
	
	private BigDecimal valorEntrada;
	private Integer qtdeParcelas;
	
	public BigDecimal getValorEntrada() {
		if(valorEntrada == null) {
			valorEntrada = new BigDecimal(0);
		}
		return valorEntrada;
	}
	
}

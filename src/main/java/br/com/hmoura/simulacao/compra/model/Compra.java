package br.com.hmoura.simulacao.compra.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Compra {

	private Produto produto;
	private CondicaoPagamento condicaoPagamento;
	
	public BigDecimal getValorParaParcelar() {
		BigDecimal valorParaParcelar = new BigDecimal(0);
		
		if(produto != null && condicaoPagamento != null) {
			BigDecimal valorProduto = this.getProduto().getValor();
			BigDecimal valorEntrada = this.getCondicaoPagamento().getValorEntrada();
			valorParaParcelar = valorProduto.subtract(valorEntrada);
		}
		return valorParaParcelar;
	}
}

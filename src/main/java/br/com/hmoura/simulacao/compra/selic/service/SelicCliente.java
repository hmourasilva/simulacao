package br.com.hmoura.simulacao.compra.selic.service;


import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.hmoura.simulacao.compra.selic.model.Selic;

@FeignClient(url="https://api.bcb.gov.br/dados/serie/bcdata.sgs.4390/dados/ultimos/", name = "selic")
public interface SelicCliente {
	
	@GetMapping("{n}?formato=json")
	public List<Selic> buscaValores(@PathVariable("n") int n);
	
	@GetMapping(value = "1?formato=json", consumes = "application/json")
	public List<Selic> buscaValoreAtual();
}

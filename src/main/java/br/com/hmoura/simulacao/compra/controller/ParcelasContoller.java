package br.com.hmoura.simulacao.compra.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.Serializable;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hmoura.simulacao.compra.model.Compra;
import br.com.hmoura.simulacao.compra.model.Parcela;
import br.com.hmoura.simulacao.compra.service.ParcelasServico;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/v1/api/pagamento/parcelas")
public class ParcelasContoller implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private ParcelasServico parcelaServico;
	
	@PostMapping(produces = APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Calcula com juros os valores das parcelas", response = Parcela[].class) 
	public ResponseEntity<?> search(@Valid @RequestBody Compra compra) {
		return new ResponseEntity<>(parcelaServico.calulaParcelas(compra), HttpStatus.CREATED);
	}
}
